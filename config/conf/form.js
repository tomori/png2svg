var path = require('path')

exports.default = {
	multipart: true,
	formidable: {
        // hash: false,
		hash: 'sha1',
        uploadDir: path.resolve(__dirname, '../../tmp')
	}
}