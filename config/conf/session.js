var redisConf = getConfig().redis,
    redisStore = require('koa-redis')
exports.default = {
    sessionKeys: ['svg2png'],
    key: 'psvg.sid',
    prefix: 'psvg:ss',
    store: redisStore({
        host: redisConf.host,
        port: redisConf.port,
        auth_pass: redisConf.password
    })
}