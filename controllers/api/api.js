'use strict'
var logger = global.getLogger('api'),
    path = require('path'),
    fs = require('fs'),
    archiver = require('archiver'),
    spawn = require('child_process').spawn

// 归档
var toArchive = function(dest, attachments) {
    var outputPath = path.join(dest, '/svg_' + Date.now() + '.zip'),
        outputStream = fs.createWriteStream(outputPath),
        archive = archiver('zip')

    archive.pipe(outputStream)
    attachments.forEach(function (item) {
        archive.append(fs.createReadStream(item), {name: path.basename(item)})
    })
    archive.finalize()
    return new Promise(function(resolve, reject) {
        outputStream.on('close', function() {
            resolve(outputPath)
        })
        archive.on('error', function(error) {
            logger.error(error)
            reject(error)
        })
    })
}

var walk = function(dir,ext) {
    ext = [].concat(ext)
    return fs.readdirSync(dir).filter(function(filename) {
        return ext.indexOf(path.extname(filename)) !== -1
    }).map(function(filename) {
        return path.resolve(dir, filename)
    })
}

var rmDirSync = function(dir) {
    fs.readdirSync(dir).forEach(function(file) {
        fs.unlinkSync(path.resolve(dir, file))
    })
    fs.rmdirSync(path.resolve(dir))
}

var csrfVerfiy = function*(next) {
    try {
        this.assertCSRF(this.request.body.fields)
        yield next
    } catch(err) {
        logger.error(err)
    }
}

module.exports = function(api) {
    api.post('/', csrfVerfiy, function*() {
        var body = this.request.body,
            self = this,
            files = body.files

        if (files.files) {
            files = [].concat(files.files)
            let tmpPath = fs.mkdtempSync('./tmp/')
            files.forEach(function(file) {
                var dest = path.join(tmpPath, file.name)
                fs.renameSync(file.path, dest)
            })
            yield new Promise(function(resolve, reject) {
                var task = spawn('sh', ['./task.sh', tmpPath])
                task.on('close', function() {
                    // 打包
                    // 定时任务删除
                    toArchive('./archivers', walk(tmpPath, '.svg')).then(function (pkg) {
                        // 删除临时文件夹
                        rmDirSync(tmpPath)
                        self.set({
                            'Content-Disposition': `attachment; filename=${path.basename(pkg)}`
                        })
                        self.type = 'zip'
                        self.body = fs.createReadStream(pkg)
                        resolve()
                    }, function(err) {
                        self.body = 'error'
                        reject(err)
                    })
                })
                task.stdout.on('data', function(data) {
                    logger.info(data.toString())
                })
                task.stderr.on('data', function(error) {
                    logger.error(error.toString())
                    self.body = 'error'
                    reject(error)
                })
            })
        }
    })
}
