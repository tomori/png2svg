path=$1
for file in $path/*
do
    if test -f $file
    then
        ext=${file##*.}
        if [ $ext = 'png' ]
        then
            bname=$(basename $file)
            pngtopnm -mix $file > $path/${bname}.pnm && potrace $path/${bname}.pnm -s -o $path/${bname}.svg
        fi
    fi
done